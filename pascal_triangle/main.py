def azr(num):
    nul = 1
    while num >= 1:
        num2 = num - 1
        a = num * num2
        ans = a * nul
        nul = ans
        num = num - 2
    return nul


# C n/k = ((n!)/((n-k)*k!)

r = int(input('input the range ...'))
for n in range(r):
    print('n', n, '\n')
    N = int(azr(n))
    for k in range(n):
        K = int(azr(k))
        nk = int(azr(n - k))
        c = int(N / (nk * K))
        print(int(c))
    print("\n")
